/*
 * Napisati program koji u odvojenom worker thread-u kreira niz
 * brojeva a potom u drugom worker thread-u računa kvadrat
 * elemenata niza.
 */

const { Worker } = require("worker_threads");

const generator = new Worker("./generator.js", { workerData: { brojElemenata: 10000000 } });
console.log("Kreiran worker");
console.log("Racunanje u toku....");

generator.on("message", r => {
    const kvadrat = new Worker("./kvadrat.js", { workerData: { niz: r } });
    kvadrat.on("message", r => console.log(r));
});

generator.on("exit", code => {
    console.log(code);
});