const { workerData, parentPort } = require("worker_threads");

function kvadriraj(niz) {
    return niz.map(x => x * x);
}

parentPort.postMessage(
    kvadriraj(workerData.niz)
);